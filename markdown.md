name: inverse
layout: true
class: inverse

---

background-image: url(img/kotynabarach.jpg)
background-size: 100%
background-repeat: no-repeat
background-position: center top
class: bottom middle

#### Migrate your company to microservices: a step by step tutorial 

by Jakub Nabrdalik

---
layout: false


## My context

21 years commercial exp

12 years designing & bulding monoliths

9 years designing & bulding microservices

Helping companies migrate from monoliths to microservices.

13+ years consulting, lecturing, giving workshops, and generally working with teams from different companies

---

class: middle

> All happy families are alike

> each unhappy family is unhappy in its own way.

> [Leo Tolstoy "Anna Karenina"]

---

## Why do they want to migrate to microservices?

#### Because our system is not fast enough

#### Because our system is a mess

#### Because we have lots of devs and productivity is low

--

3rd requires microservices, the other two do not

???

Only the third one is a good reason to migrate

But often all those three come together

---

## Top down or bottom up?

For an example of a messy but succesfull "all-in" approach see:

[Ten Years and Counting: My Affair with Microservices - Michał Kosmulski](https://blog.allegro.tech/2024/04/ten-years-microservices.html)

Do not emulate. This was before we knew how to tackle this problem.r

---

## My recommended approach

#### Give a working example

Ensure microservices are possible

Build a working-on-production example within the company

--

#### Make it easy

Ensure making microservices is easy
- tech changes
- operations changes
- mgm changes
- culture changes

--

#### Help product teams

Help others design microservices, 

Educate teams

---

## Step 1: design initial architecture of the new system

You don't need to design the full system

You will use the Strangler Pattern and extract process by process anyway

If the system is too large to put into your head, do not worry

Do not stick to the initial design. 

In my 21 years of experience, I've never built the system the way I have desgined it, so don't overdo it

---

### Find out what’s going to change the most 

If they tell you they want microservices or Event Driven Architecture, those are solutions, not the problem.

Ask about the problem?

Can we put numbers on it?
- time to market of a new feature
- throughput of a process
- devs getting mad

---

### Pair with someone who knows current code/processes

Designing is easy (1 day for a complex process or subsystem)

Understanding HOW it works right now is hard

Understanding HOW it is supposed to work (after changes) takes time

Get someone who knows the current code inside-out

Ask people to prepare sequence diagrams of current stuff, or comment the code.

If nobody seems to understand the process, then do Event Storming

Choose the process that has the most changes planned

Use the Strangler Pattern to extract it

---

### Design: Make it work, make it fast, make it manageable

If your process needs low latency, you need data locality

--

Just kidding, you ALWAYS need data locality

--

Count number of I/O operations, this is where your process spends the most time

--

When it's fast enough, optimize for manageability and extensibility

--

Forces that impact your design most often
- code ownership
- data locality & I/O operations
- transport cost (including de/serialization)
- frequency of change vs uptime vs SLA
- security

---

### Keep the design as code in git

Unlike with a monolith you will have several repos, so create another one for architecture

Push Structurizr Lite (https://github.com/structurizr/lite) as a project there

You can also use PlantUML, IDEs have plugins to render that

Design c4 as code in dsl

Mermaid handles C4 in Readme, but I do not recommend it (not production ready yet imho)

---

## Step 2: tailored service template (TST) + 1st microservice

The goal is to allow you to focus on business code as fast as possible.

And take away laziness from the decision "should I put this new module into an existing microservice, or build a new one"

--

We want to have a new microservice in 5 seconds, with:
- testing setup (with examples for unit and integrations tests)
- testcontainers: Kafka and DB
- parallel test runners
- (transactional) outbox(es)
- kafka read model example
- readme template (important)
- pipelines with CD to production
- zero downtime deployment

---

### Evolution of Tailored Service Template

1. an example repo and a script to setup new microservice in 5 sec

2. customization options in script where you choose libraries and frameworks, and that prepares the repo for you

3. a web page / configurator like start.spring.io, where you choose everything you want

---

### Example: shell script setup.sh

```bash
#!/bin/bash

if [ -z $1 ]; then
    echo please provide repo name
    exit
fi

git clone <your git repo>/$1.git
cd $1

git remote add template <your git repo>/tailored-service-template.git
git pull template main
git push origin main

./gradlew setup -PnameOfNewService=$1

git add --all
git commit -m 'CI/CD setup'
git push origin main
```

---

### Example: template Gradle task

```kotlin
tasks.create("setup") {
   doLast {
      val newServicePropertyName = "nameOfNewService"
      val nameOfNewService: String = getName(newServicePropertyName)
      println("Setting up a new service with name \"$nameOfNewService\"")
      moveFile(".gitlab-ci-template.yml",".gitlab-ci.yml")
      moveFile("README-template.md", "README.md")
      replaceMarkerInFile(".gitlab-ci.yml", nameOfNewService)
      replaceMarkerInFile("README.md", nameOfNewService)
      replaceMarkerInFile("./src/main/resources/banner.txt", nameOfNewService)
      replaceMarkerInFile("./src/main/java/kafka/KafkaNames.java", nameOfNewService)
      replaceMarkerInFile("./settings.gradle.kts", nameOfNewService)
      replaceMarkerInFile("./src/main/resources/application.yml", nameOfNewService)
   }
}
```

This shit is easy!


---

### Template: libs or code within?

You can create stuff as libs, or within the template. When to create which?

Libraries - harder to change (require releases) - avoid customizable code

Code in template - harder to upgrade existing microservice - avoid code that changes often

Important - make many small libraries, not a single "common" library. 

For REST based communication I'd have separate libs for
- user authentication
- microservice authentication
- traffic logging filters

Make libs depend on as little as possible (frameworks etc.).

---

### Template: Outbox pattern

(Transactional) outbox allows you to do retries and compensation.

Types of transactional outbox to prepare:

Slow outbox, easy to debug, 100% idiot proof
> save in DB, send in another thread (@Schedulled)

Fast outbox, no ordering guarantees, 99% idiot proof
> send in the same thread, save in DB on failure & commit, retry with @Schedulled

Outbox with ordering guarantees per key, 98% idiot proof
> partitioning data on save or send; stopping for given key on first error; never stopping the whole stream processing

--

When using kafka + Spring, in & out, @RetryableTopic is your friend

---

### Template: one or many

How many templates do we need?

- one per framework? 
- one per team?

The work spent on the first one will not be lost, it help create new ones.

---

## Step 3: continuous deployment

Get those pipeline working after every merge to main.

--

Tests? Left shift everything.

--

Manual approvals? That's the definition of PR code review.

--

Keep properties within the microservice

Keep secrets in vault

--

Make sure your liquibase/flyway script work in pipeline with a different user than the app.

The app SHOULD NOT have a DB user with DDL access!

--

What if your old system has end-to-end tests? Allow but don’t rely yourself.

Deploy to all environemnts in parallel + prod after one env (dev)

---

## Step 4: Watertight Compartments (infra isolation)

Design with bulkheads (separation of failure) in mind

How can a team impact another team via:
- shared DB servers
- shared k8s cluster (nodes)
- shared event bus (kafka)
- shared logging aggregator
- shared application gateway
- shared frontend

Which of those you can make invulnerable to other teams?

Ingress + application gateway (keep traffic rules within microservice)

---

## Step 5: kafka (or event bus of any kind)

Setup topics via pipeline (do not leave kafka admin API)

Topic namings (public vs private vs team based)

Public kafka vs kafka per team

---

## Step 6: security


Access to k8s read only (kubectl, lens)

--

All k8s changes via pipeline

--

Turn off Kafka Admin API

--

Lens (topic visibility): lenses.io

--

DB access (different trust levels is ok)

--

Sec person joining your team (threat modeling)

--

Baseline, one off tasks - cronjob fired by pipeline (full auditing, easy to do)

---

## Step 7: monitoring & observability

Dashboards: tech and business metrics

--

Make people use them, explain everyday the situation on production

--

24/7 on duty and readme

--

Smoke tests on prod

---

## Step 8: knowledge governance

Microservices require decentralized governance

Don’t use tools for noobs (confluence etc.). 

Migrate your knowledge to git

Obsidian (https://obsidian.md) is a nice tool for knowledge governance via git

Architecture repo is cool

Automated tools for documentation are cool

Developer portal when big enough

https://backstage.io/

https://www.jetbrains.com/writerside/


---

## Step 9: Autonomous Product teams 

All decisions about the team, including the backlog, are made within the team

--

Managers are now a Venture Capital

Teams are startups

Management funds a product and funds a team

No gant charts, no moving people between teams, no classic "people management", no micromanagement

Requirements come from YOU (the team) based on your customer needs (your business). Not from managers

---


### What decisions management makes?

Do we need to create a new product? (a bet)

Do we need a new team for the product or can we give it to existing one? (estimation)

Do we keep funding the product or should it be abandoned? (evaluation)

Do we keep funding the team or should it be disbanded? (evaluation)

--

STOP REPORTING TIME SPENT (EFFORT)

It's much better to do very little with large impact on your customers/business than to do a lot of work that doesn't help anyone.

---

### What decisions are within the team?

A Team:

Spends the budget (how many people, infra, how much research, team composition)

Controls the vision and strategy (decides what they want to build, and how to achieve that)

Controls the backlog (has a final say in what the product will do, and what it will not do)

Applies for more funding

---

### How do you create a product team?

Find a person you trust (team leader / product owner)

Management sponsors a product by giving it a budget

The teams presents progress with working software on production (that's all the reporting you get)

Management decides whether to continue funding the team or not

Think about it as "a startup you fund". 

You do not look at the daily works of the standup, you are not part of the standup. You are an investor and you react only when things go wrong.

---

### This is no system for young men

Skills required in a team to do microservices

--

Programming in 1-4 languages

Requirement refinement (analysis), use cases, behavioral scenarios

Writing tests (TDD + BDD + Pefromance/Stress tests)

Operating kubernetes (kubectl, lens, etc.)

Writing / modifying pipelines in a given environment

Observability: what/how to log, metrics, traces

Building and using dashboards (Datadog, Application Insights, etc.)

Remote debugging

Architecture

Kafka partitioning and communication patterns

Documentation (technical writing, communication)

Scrum (or methodology knowledge/experience)


---

### Buy what you don't have

Could we use external help, and not have the person 100% of the time?

Security expert

QA expert

Architect

Scrum expert

SRE/Infra/k8s/kafka expert

If you have an expert: make her educate people (one to many).

Internal workshops, courses, recordings, etc.

---

## Step 10: real architecture

Design with teams in mind

Context mapping vs minimizing cognitive load

---

## Step 11: Consumer Driven Contracts

Forget Schema Registry for Kafka

Use Pact (https://pact.io/) or Spring Cloud Contract

Do not write contracts between microservices of the same team. Those contracts do not break (if they ever will, fire developers)

---

## Step 12: Enjoy

Microservices reduce stress and improve productivity

Introduce more tech complexity, more learning, more tools

Not everyone wants to be more productive, so not everyone will be happy

But we still need people to keep strangled old monoliths operational

So there is place for everyone

---

class: center middle


# Good luck

Twitter @jnabrdalik

jakubn@gmail.com 

https://nabrdalik.dev/

This presentation is available at <br />
https://jakubn.gitlab.io/migrate2microservicesstepbystep


If you want a workshop on architecture: <br />
https://nabrdalik.dev/workshops/ https://bottega.com.pl/

More on architecture: 

What I wish I knew when I started designing systems <br />
https://www.youtube.com/watch?v=4Iqjhi3kusY

Common mistakes when moving to microservices <br /> 
https://www.youtube.com/watch?v=6H3mSY1AJ1k

